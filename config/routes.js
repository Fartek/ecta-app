module.exports.routes = {
  '/': {
    controller: 'home',
    action: "default",
  },
  '/ask': {
    controller: 'ask',
    action: "default",
  },

  '/offer': {
    controller: 'offer',
    action: "default",
  },

  '/projects': {
    controller: 'projects',
    action: "default",
  },
};
