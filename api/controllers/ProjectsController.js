/**
 * ProjectsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    default: function(req, res) {
        return res.view('pages/projects', {
          status: 'OK',
          title: 'What is this application all about ?'
        });
      }
};

