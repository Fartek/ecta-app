/**
 * AskController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  default: function(req, res) {
    console.info('default invoked');
    return res.view('pages/ask', {
      status: 'OK',
      title: 'What is this application all about ?'
    });
  }
};
